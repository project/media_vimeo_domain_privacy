Media Vimeo Domain Privacy
-------

This module provides support for adding Vimeo videos with a domain-level privacy
setting as media entities, by providing a `Referer` value in the oEmbed request.
Without this module, you cannot create remote video entities for Vimeo videos
with domain-level privacy settings.

INTRODUCTION
------------

This module was created because the data provided via the oEmbed API for Vimeo
videos with a domain-level privacy is restricted. To get the complete response,
including the private metadata, you must send the `Referer` header with the
request, and set its value to the video's whitelisted domain.

An `http_client_middleware` service is provided which adds the current host as
the `Referer` to the request so these Vimeo videos can be created as media
entities. For those who are interested, another example of an
`http_client_middleware` service is `TestHttpClientMiddleware`.

For additional information, see the [original issue](https://www.drupal.org/project/drupal/issues/3056124).

REQUIREMENTS
------------

There are no special requirements to use this module.

RECOMMENDED MODULES
-------------------

There are no recommended contributed modules to use this module.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   [Installing Modules](https://www.drupal.org/node/1897420) for further
   information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will allow Vimeo videos with domain-level privacy settings
to be used.

Note, currently this module does not support decoupled websites where the host
is not the same as the whitelisted domain. In the future, a setting could be
added to specify the domain to use for the `Referer` to support decoupled sites.

TROUBLESHOOTING
---------------

If the video does not show up, check your Vimeo domain settings and compare them
against the site's domain.

CONTRIBUTORS
------------

Original Code Contributor:
 * Dave Long - https://www.drupal.org/u/longwave

Module name and README Contributor:
 * Pamela Barone - https://www.drupal.org/u/pameeela

Additional Contributors (alphabetical order):
 * Adam G-H - https://www.drupal.org/u/phenaproxima
 * Alex Bronstein - https://www.drupal.org/u/effulgentsia
 * Alex McCracken - https://www.drupal.org/u/gcalex5
 * Chris Matthews - https://www.drupal.org/u/chris-matthews
 * Damir - https://www.drupal.org/u/damir
 * Jeroen Tubex - https://www.drupal.org/u/jeroent
 * Jon Holmes - https://www.drupal.org/u/jlholmes
 * Joseph Zhao - https://www.drupal.org/u/pandaski
 * Julian Pustkuchen - https://www.drupal.org/u/anybody
 * Lee Rowlands - https://www.drupal.org/u/larowlan
 * Mikko Rantanen - https://www.drupal.org/u/mikran
 * Paul Driver - https://www.drupal.org/u/mrpauldriver
 * And others - https://www.drupal.org/project/drupal/issues/3056124

MAINTAINERS
-----------

Current maintainers (alphabetical order):
 * Joshua Fernandes - https://www.drupal.org/u/joshua1234511
 * Kristen Pol - https://www.drupal.org/u/kristen-pol

This project is sponsored by:
 * SALSA DIGITAL - Salsa Digital is a digital agency focused on open source for
   social good and innovation.
   Visit [Salsa's website](https://salsadigital.com.au/) for more information.
