<?php

namespace Drupal\media_vimeo_domain_privacy\HttpClientMiddleware;

use Psr\Http\Message\RequestInterface;

/**
 * Vimeo Domain Privacy Middleware.
 *
 * This is a http_client_middleware service that adjusts the referrer for Vimeo
 * videos so that videos with domain-level privacy will render properly.
 *
 * @see https://www.drupal.org/project/drupal/issues/3056124#comment-14214280
 */
class VimeoDomainPrivacyMiddleware {

  /**
   * Adds referrer to request to handle Vimeo videos with domain-level privacy.
   */
  public function __invoke() {
    return function ($handler) {
      return function (RequestInterface $request, array $options) use ($handler) {
        if (preg_match('/\.?vimeo\.com$/', $request->getUri()->getHost())) {
          // Vimeo videos with domain-level privacy require the referer be set
          // in the header to the whitelisted domain.
          // @todo Add referer domain setting to support decoupled sites.
          $referrer = \Drupal::request()->getSchemeAndHttpHost() . '/';
          $request = $request->withHeader('Referer', $referrer);
        }
        return $handler($request, $options);
      };
    };
  }

}
